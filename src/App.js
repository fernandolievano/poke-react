import React, { Component } from 'react';
import './App.scss';

//components
import PokemonList from './components/PokemonList'

class App extends Component {
  render() {
    return (
      <div>
        <main>
          <PokemonList />
        </main>
      </div>
    );
  }
}

export default App;
