import React, { Component } from 'react'
import axios from 'axios'
import '../App.scss'

//components
import Pokemon from './Pokemon'

class PokemonList extends Component {
  state = { list: {} }

  async componentDidMount() {
    try {
      const response = await axios.get(
        'http://pokeapi.co/api/v2/pokemon/?limit=12'
      )
      const data = response.data.results
      // console.log(data)
      this.setState({ list: data })
    } catch (error) {
      console.error(error)
    }
  }

  _renderList() {
    const pokemon = this.state.list
    const pokemonList = Object.keys(pokemon)

    return pokemonList.map(index => {
      const name = pokemon[index].name
      const url = pokemon[index].url

      return (
        <div key={index} className="column is-4-tablet">
            <Pokemon name={name} url={url} />
        </div>
      )
    })
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <h1 className="is-size-1">Pokémon List</h1>
          <hr />
          <div className="columns is-multiline is-mobile">
            {this._renderList()}
          </div>
        </div>
      </section>
    )
  }
}

export default PokemonList
