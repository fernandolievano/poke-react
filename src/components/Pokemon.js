import React, { Component } from 'react'
import axios from 'axios'
import '../App.scss'

//components
import Sprites from './Sprites'

function Types(props) {
  return props.types.map((type, index) => {
    return <span key={type.type.name+index} className="tag is-primary">{type.type.name}</span>
  })
}

class Pokemon extends Component {
  constructor(props) {
    super(props)
    this.state = { nDex: null, pokemon: {}, sprites: {}, types: [] }
  }

  async componentDidMount() {
    const url = this.props.url

    const response = await axios.get(url)

    this.setState({
      pokemon: response.data,
      sprites: response.data.sprites,
      types: response.data.types,
      nDex: response.data.order
    })
  }

  render() {
    const pokemon = this.props.name

    return (
      <div className="card">
        <header className="card-header">
          <h2 className="capitalize card-header-title is-centered">
            #{this.state.nDex}  {pokemon}
          </h2>
        </header>
        <div className="card-content has-text-centered">
          <Sprites sprites={this.state.sprites} />
        </div>
        <footer className="card-footer">
          <div className="card-footer-item">
            <Types types={this.state.types} />
          </div>
        </footer>
      </div>
    )
  }
}

export default Pokemon
