import React, { Component } from 'react'
import '../App.scss'

class Sprites extends Component {
  render() {
    const sprites = this.props.sprites

    return (
      <div className="columns is-mobile is-horizontal-center">
        <div className="is-half sprites-container sprite-card">
          <img
            className="shiny-sprite"
            src={sprites.front_shiny}
            alt=""
          />
          <img
            className="default-sprite"
            src={sprites.front_default}
            alt=""
          />
        </div>
        <div className="is-half sprites-container sprite-card">
          <img
            className="default-sprite"
            src={sprites.back_default}
            alt=""
          />
          <img
            className="shiny-sprite"
            src={sprites.back_shiny}
            alt=""
          />
        </div>
      </div>
    )
  }
}

export default Sprites